-- DROP TABLE anagrama.palabras;

CREATE TABLE anagrama.palabras
(
    id serial NOT NULL,
    palabra text NOT NULL,
    activo boolean NOT NULL DEFAULT TRUE,
    CONSTRAINT pk_palabras PRIMARY KEY (id),
    CONSTRAINT unq_palabras_palabra UNIQUE (palabra)
);

INSERT INTO anagrama.palabras(palabra) VALUES('CABRON');
INSERT INTO anagrama.palabras(palabra) VALUES('BRONCA');
INSERT INTO anagrama.palabras(palabra) VALUES('CARBON');
INSERT INTO anagrama.palabras(palabra) VALUES('ALGO');
INSERT INTO anagrama.palabras(palabra) VALUES('GOLA');
INSERT INTO anagrama.palabras(palabra) VALUES('GALO');
INSERT INTO anagrama.palabras(palabra) VALUES('LAGO');
INSERT INTO anagrama.palabras(palabra) VALUES('OLGA');
INSERT INTO anagrama.palabras(palabra) VALUES('ALERGIA');
INSERT INTO anagrama.palabras(palabra) VALUES('ALEGRIA');
INSERT INTO anagrama.palabras(palabra) VALUES('GALERIA');
INSERT INTO anagrama.palabras(palabra) VALUES('ACTO');
INSERT INTO anagrama.palabras(palabra) VALUES('TOCA');
INSERT INTO anagrama.palabras(palabra) VALUES('COTA');
INSERT INTO anagrama.palabras(palabra) VALUES('TACO');
INSERT INTO anagrama.palabras(palabra) VALUES('ARTEFACTOS');
INSERT INTO anagrama.palabras(palabra) VALUES('CATASTROFE');
INSERT INTO anagrama.palabras(palabra) VALUES('ASTILLERO');
INSERT INTO anagrama.palabras(palabra) VALUES('LITORALES');
INSERT INTO anagrama.palabras(palabra) VALUES('CATRE');
INSERT INTO anagrama.palabras(palabra) VALUES('TERCA');
INSERT INTO anagrama.palabras(palabra) VALUES('CRETA');
INSERT INTO anagrama.palabras(palabra) VALUES('CERO');
INSERT INTO anagrama.palabras(palabra) VALUES('ROCE');
INSERT INTO anagrama.palabras(palabra) VALUES('OCRE');
INSERT INTO anagrama.palabras(palabra) VALUES('CORNEA');
INSERT INTO anagrama.palabras(palabra) VALUES('CRANEO');
INSERT INTO anagrama.palabras(palabra) VALUES('NECORA');
INSERT INTO anagrama.palabras(palabra) VALUES('CUARTEL');
INSERT INTO anagrama.palabras(palabra) VALUES('RECLUTA');
INSERT INTO anagrama.palabras(palabra) VALUES('GRAPA');
INSERT INTO anagrama.palabras(palabra) VALUES('PRAGA');
INSERT INTO anagrama.palabras(palabra) VALUES('PAGAR');
INSERT INTO anagrama.palabras(palabra) VALUES('HORCA');
INSERT INTO anagrama.palabras(palabra) VALUES('CHARO');
INSERT INTO anagrama.palabras(palabra) VALUES('GORILA');
INSERT INTO anagrama.palabras(palabra) VALUES('GLORIA');
INSERT INTO anagrama.palabras(palabra) VALUES('IMPRESO');
INSERT INTO anagrama.palabras(palabra) VALUES('PREMIOS');
INSERT INTO anagrama.palabras(palabra) VALUES('PERMISO');
INSERT INTO anagrama.palabras(palabra) VALUES('MACABRA');
INSERT INTO anagrama.palabras(palabra) VALUES('CARAMBA');
INSERT INTO anagrama.palabras(palabra) VALUES('MORA');
INSERT INTO anagrama.palabras(palabra) VALUES('AMOR');
INSERT INTO anagrama.palabras(palabra) VALUES('ROMA');
INSERT INTO anagrama.palabras(palabra) VALUES('NOTAR');
INSERT INTO anagrama.palabras(palabra) VALUES('RATON');
INSERT INTO anagrama.palabras(palabra) VALUES('ROTAN');

SELECT * FROM anagrama.palabras;

SELECT id, palabra, activo FROM anagrama.palabras 
WHERE PALABRA NOT IN('AMOR', 'ROMA') 
ORDER BY random() LIMIT 1;

SELECT id FROM anagrama.palabras WHERE UPPER(palabra) = UPPER('ROMAS');