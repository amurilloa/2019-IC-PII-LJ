/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anagrama.dao;

import anagrama.entities.Palabra;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class AnagramaDAO {

    
    public LinkedList<Palabra> cargarPalabras() {
        LinkedList<Palabra>  palabras = new LinkedList<>();
        
        try (Connection con = Conexion.conexion()) {
            String sql = "SELECT id, palabra, activo"
                    + " FROM anagrama.palabras";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                palabras.add(cargarPalabra(rs));
            }

        } catch (Exception ex) {
            throw new RuntimeException("Palabras no encontradas, "
                    + "favor intente nuevamente");
        }
        
        return palabras;
    }
    
    public Palabra cargarPalabra() {
        try (Connection con = Conexion.conexion()) {
            String sql = "SELECT id, palabra, activo"
                    + " FROM anagrama.palabras"
                    + " ORDER BY random() LIMIT 1";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return cargarPalabra(rs);
            }

        } catch (Exception ex) {
            throw new RuntimeException("Palabras no encontradas, "
                    + "favor intente nuevamente");
        }
        return null;
    }

    public void insertarPalabra(Palabra p) {
        try (Connection con = Conexion.conexion()) {
            String sql = "INSERT INTO anagrama.palabras(palabra) values(?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, p.getPalabra());
            ps.executeUpdate();
        } catch (Exception ex) {
            throw new RuntimeException("Problemas al guardar la palabra");
        }
    }

    private Palabra cargarPalabra(ResultSet rs) throws SQLException {
        Palabra temp = new Palabra();
        temp.setId(rs.getInt("id"));
        temp.setPalabra(rs.getString("palabra"));
        temp.setActivo(rs.getBoolean("activo"));
        return temp;
    }

    public boolean validar(String palabra) {
        try (Connection con = Conexion.conexion()) {
            String sql = "SELECT id "
                    + " FROM anagrama.palabras "
                    + " WHERE UPPER(palabra) = UPPER(?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, palabra);
            ResultSet rs = ps.executeQuery();
            return rs.next();

        } catch (Exception e) {
            e.printStackTrace();
//            throw new RuntimeException("Palabras no encontradas, "
//                    + "favor intente nuevamente");
        }
        return false;
    }
}
