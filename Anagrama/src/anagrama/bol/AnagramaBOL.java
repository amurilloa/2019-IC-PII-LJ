/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anagrama.bol;

import anagrama.dao.AnagramaDAO;
import anagrama.entities.Palabra;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class AnagramaBOL {

    private Palabra palabraSel;
    private AnagramaDAO ado;

    public AnagramaBOL() {
        ado = new AnagramaDAO();
    }

    public boolean revisar(String palabra) {
        if (palabra == null || palabraSel == null || palabraSel.getPalabra() == null) {
            return false;
        }
        palabra = palabra.toUpperCase();
        if (palabraSel.getPalabra().equalsIgnoreCase(palabra)) {
            return true;
        }
        if (palabraSel.getPalabra().length() != palabra.length()) {
            return false;
        }
        //Si todas las letras que estan en la palabra seleccionada están en el 
        //string que digitó el usuario
        String[] letras = palabraSel.getPalabra().split("");
        for (String letra : letras) {
            if (!palabra.contains(letra)) {
                return false;
            }
        }
        return ado.validar(palabra); //Ir a la base de datos y revisar contra las otras palabras similares
    }

    /**
     * Desordena la palabra seleccionada
     *
     * @return String con la palabra desordenada
     */
    public String getDesPalabra() {
        cargarPalabra();
        StringBuilder sb = new StringBuilder();
        if (palabraSel != null) {
            String[] arr = palabraSel.getPalabra().split("");
            LinkedList<String> letras = new LinkedList<>(Arrays.asList(arr));
            Collections.shuffle(letras);
            for (String letra : letras) {
                sb.append(letra);
            }
        }
        return sb.toString().toUpperCase();
    }

    private void cargarPalabra() {
        palabraSel = ado.cargarPalabra();
        System.out.println(palabraSel.getPalabra());
    }

}
