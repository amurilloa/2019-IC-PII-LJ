/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

import herencia.Alumno;

/**
 *
 * @author allanmual
 */
public class Main {

    public static void main(String[] args) {
        Alumno a1 = new Alumno();
        Guerrero g1 = new Guerrero("Axe", "Hacha", 200);
        Mago m1 = new Mago("Harry", "Borbujitas de Jabón");
        System.out.println(g1.combatir(55));
        System.out.println(m1.encantar());
        System.out.println(m1.encantar());
        System.out.println(m1.encantar());
        System.out.println(m1.encantar());
        System.out.println(g1.combatir(55));
        System.out.println(g1.combatir(55));
        System.out.println(g1.combatir(55));

        Personaje p2 = new Mago("Gandalf", "Luz...");
        Personaje p1 = new Guerrero("Rey Arturo", "Escalibur", 1000);
        Personaje p3 = new Personaje("Conejo", 20);
        
        
        if (p1 instanceof Mago) {
            System.out.println(((Mago) p1).encantar());
        } else if (p1 instanceof Guerrero) {
            System.out.println(((Guerrero) p1).combatir(100));
        } else {
            System.out.println(p1);
        }

        int cant =0;
        Object[] objetos = {1, "Hola", 'c', p1, p3, p2};
        for (Object objeto : objetos) {
            if(objeto instanceof Guerrero){
                cant++;
            }
        }
        System.out.println("Cant. Guerreros: " + cant);
        // private, public, protected

    }
}
