/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

/**
 *
 * @author allanmual
 */
public class Mago extends Personaje {

    private String poder;

    public Mago(String nombre, String poder) {
        super(nombre, 100);
        this.poder = poder;
    }

    public String encantar() {
        alimentarse(-2);
        return poder;
    }

    public String getPoder() {
        return poder;
    }

    public void setPoder(String poder) {
        this.poder = poder;
    }

    @Override
    public String toString() {
        return "Mago{" + "poder=" + poder + '}';
    }

}
