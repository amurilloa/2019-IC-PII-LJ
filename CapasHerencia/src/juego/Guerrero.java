/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

/**
 *
 * @author allanmual
 */
public class Guerrero extends Personaje {

    private String arma;

    public Guerrero(String nombre, String arma, int energia) {
        super(nombre, energia);
        this.arma = arma;
    }

    public String combatir(int energia) {
        if (getEnergia() >= energia) {
            alimentarse(-energia);
            return arma + "-" + energia;
        } 
        return "Sin energía!!";
    }

    public String getArma() {
        return arma;
    }

    public void setArma(String arma) {
        this.arma = arma;
    }

    @Override
    public String toString() {
        return "Guerrero{" + "arma=" + arma + '}';
    }
}
