/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author allanmual
 */
public class Main {

    public static void main(String[] args) {
        Alumno a1 = new Alumno(123, "Allan", "ISW");
        //Persona p = new Persona(124, "Juan");
        //System.out.println(a.quienSoy());
        System.out.println(a1.indentificarse());
        //System.out.println(p.indentificarse());
        System.out.println(a1);

        Autobus aut1 = new Autobus(60, "HBX-692", "Toyota");
        Camion cam1 = new Camion(12, "HBX-546", "Isuzu");
        CamionCompartimientos cam2 = new CamionCompartimientos(10, 80, "HBX-342", "Isuzu");
        System.out.println(cam2.capacidadCompartimiento());
        System.out.println(cam2.descripcion());

//        System.out.println(a.toString());
        Object[] arreglo = {1, "", 'a', true, a1, cam2};

        Vehiculo v1 = new Autobus(60, "HBX-692", "Toyota");
        Vehiculo v2 = new Camion(12, "HBX-546", "Isuzu");
        Vehiculo v3 = new CamionCompartimientos(10, 80, "HBX-342", "Isuzu");
        //Vehiculo v4 = new Vehiculo("HBL-344", "Nissan");
        Vehiculo[] vehiculos = {aut1, cam1, cam2, v1, v2, v3};

        System.out.println(((Camion) v2).getCarga());

        for (Vehiculo v : vehiculos) {
            if (v instanceof CamionCompartimientos) {
                CamionCompartimientos cc = (CamionCompartimientos) v;
                System.out.println(cc);
                System.out.println(cc.descripcion());
                System.out.println(cc.capacidadCompartimiento());
            } else if (v instanceof Camion) {
                Camion c = (Camion) v;
                System.out.println(c);
                System.out.println(c.getCarga());
            } else if (v instanceof Autobus) {
                Autobus a = (Autobus) v;
                System.out.println(v);
                System.out.println(a.getAsientos());
            } else {
                System.out.println(v);
            }
        }

    }
}
