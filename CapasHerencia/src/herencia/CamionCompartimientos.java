/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author allanmual
 */
public class CamionCompartimientos extends Camion {

    private int compartimientos;

    public CamionCompartimientos(int compartimientos, int carga, String placa, String marca) {
        super(carga, placa, marca);
        this.compartimientos = compartimientos;
    }

    public double capacidadCompartimiento() {
        return (double) getCarga() / compartimientos;
    }

    public String descripcion() {
        return getMarca() + " : " + compartimientos;
    }

    public int getCompartimientos() {
        return compartimientos;
    }

    public void setCompartimientos(int compartimientos) {
        this.compartimientos = compartimientos;
    }

    @Override
    public String toString() {
        return "CamionCompartimientos{" + "compartimientos=" + compartimientos + '}';
    }    

}
