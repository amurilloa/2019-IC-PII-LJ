/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author allanmual
 */
public class Camion extends Vehiculo {

    private int carga;

    public Camion(int carga, String placa, String marca) {
        super(placa, marca);
        this.carga = carga;
    }

    public int getCarga() {
        return carga;
    }

    public void setCarga(int carga) {
        this.carga = carga;
    }

    @Override
    public String toString() {
        return "Camion{" + "carga=" + carga + '}';
    }

}
