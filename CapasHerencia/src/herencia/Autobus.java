/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author allanmual
 */
public class Autobus extends Vehiculo {

    private int asientos;

    public Autobus(int asientos, String placa, String marca) {
        super(placa, marca);
        this.asientos = asientos;
    }

    public int getAsientos() {
        return asientos;
    }

    public void setAsientos(int asientos) {
        this.asientos = asientos;
    }

    @Override
    public String toString() {
        return "Autobus{" + "asientos=" + asientos + '}';
    }

    
}
