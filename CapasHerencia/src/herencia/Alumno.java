/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author allanmual
 */
public class Alumno extends Persona {

    private String carrera;

    public Alumno() {
    }

    public Alumno(int id, String nombre, String carrera) {
        super(id, nombre);
        this.carrera = carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getCarrera() {
        return carrera;
    }

    @Override
    public String indentificarse() {
        return carrera + " " + id + " " + nombre;
    }

    @Override
    public String toString() {
        return super.toString() + "Alumno{" + "carrera=" + carrera + '}';
    }

    @Override
    public String saludar() {
        return "Hola!!!";
    }

}
