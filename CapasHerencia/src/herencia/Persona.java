/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author allanmual
 */
public abstract class Persona {

    protected int id;
    protected String nombre;

    //private, public, protected
    public Persona() {

    }

    public Persona(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public abstract String saludar();
    
    public abstract String indentificarse();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Persona{" + "id=" + id + ", nombre=" + nombre + '}';
    }

}
