/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplodos;

/**
 *
 * @author allanmual
 */
public class Perro {

    private String nombre;
    private String raza;
    private boolean pedigree;
    private String fechaNac;
    private String color;

    public Perro() {
    }

    public Perro(String nombre, String raza, boolean pedigree, String fechaNac, String color) {
        this.nombre = nombre;
        this.raza = raza;
        this.pedigree = pedigree;
        this.fechaNac = fechaNac;
        this.color = color;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public boolean isPedigree() {
        return pedigree;
    }

    public void setPedigree(boolean pedigree) {
        this.pedigree = pedigree;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Perro{" + "nombre=" + nombre + ", raza=" + raza + ", pedigree=" + pedigree + ", fechaNac=" + fechaNac + ", color=" + color + '}';
    }
}
