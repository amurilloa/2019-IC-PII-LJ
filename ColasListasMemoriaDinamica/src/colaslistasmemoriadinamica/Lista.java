/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colaslistasmemoriadinamica;

/**
 *
 * @author allanmual
 */
public class Lista {

    private int cant;
    private Jugador inicio;

    /**
     * Agrega al final de la lista
     *
     * @param jugador Jugador que se agrega a la lista
     */
    public void agregar(Jugador jugador) {
        if (vacia()) {
            inicio = jugador;
        } else {
            Jugador t = inicio;
            while (t.getSiguiente() != null) {
                t = t.getSiguiente();
            }
            t.setSiguiente(jugador);
        }
        cant++;
    }
    //I   
    //p1-->p5-->p4-->p2 --> p3

    /**
     * Agregar al inicio de la lista
     *
     * @param jugador Jugador que se agrega al inicio de la lista
     */
    public void agregarInicio(Jugador jugador) {
        jugador.setSiguiente(inicio);
        inicio = jugador;
        cant++;
    }

    /**
     * Determina si la lista se encuentra vacia
     *
     * @return true en caso de no tener elementos en la lista
     */
    public boolean vacia() {
        return inicio == null;
    }

    public void insertar(int pos, Jugador jug) {
        if (pos < 0 || jug == null || pos > cant) {
            throw new RuntimeException("Posición/Jugador inválidos");
        }
        if (pos == 0) {
            agregarInicio(jug);
        } else {
            Jugador temp = inicio;
            for (int i = 0; i < pos - 1; i++) {
                temp = temp.getSiguiente();
            }
            jug.setSiguiente(temp.getSiguiente());
            temp.setSiguiente(jug);
            cant++;
        }
    }

    //T 
    //p1-->p5-->p4-->p2 --> p3-->
    public void eliminar(int pos) {
        if (pos < 0 || pos >= cant) {
            throw new RuntimeException("Posición inválida");
        }
        if (pos == 0) {
            inicio = inicio.getSiguiente();
        } else {
            Jugador temp = inicio;
            for (int i = 0; i < pos - 1; i++) {
                temp = temp.getSiguiente();
            }
            temp.setSiguiente(temp.getSiguiente().getSiguiente());
        }
        cant--;
    }

    public void eliminar(Jugador jug) {
        if (inicio.equals(jug)) {
            inicio = inicio.getSiguiente();
            cant--;
        } else {
            Jugador temp = inicio;
            for (int i = 0; i < cant - 1; i++) {
                if (temp.getSiguiente().equals(jug)) {
                    temp.setSiguiente(temp.getSiguiente().getSiguiente());
                    cant--;
                    break;
                }
                temp = temp.getSiguiente();
            }
        }
    }

    public Jugador buscar(int pos) {
        if (pos < 0 || pos >= cant) {
            return null;//Si no se encuentra o el indice es mayor a la cantidad de elementos
        }
        Jugador temp = inicio;
        for (int i = 0; i < pos; i++) {
            temp = temp.getSiguiente();
        }
        return temp;
    }

    public int buscar(Jugador jug) {
        Jugador temp = inicio;
        for (int i = 0; i < cant; i++) {
            if (temp.equals(jug)) {
                return i;
            }
            temp = temp.getSiguiente();
        }
        return -1; //Cuando no logra encontrar el jugador
    }

    public int size() {
        return cant;
    }

    @Override
    public String toString() {
        String txt = "";
        Jugador actual = inicio;
        while (actual != null) {
            txt += actual.getNombre() + "(" + actual.getPuntaje() + ") -> ";
            actual = actual.getSiguiente();
        }
        return txt;
    }

}
