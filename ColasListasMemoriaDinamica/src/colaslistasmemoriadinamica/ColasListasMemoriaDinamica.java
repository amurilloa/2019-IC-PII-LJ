/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colaslistasmemoriadinamica;

/**
 *
 * @author allanmual
 */
public class ColasListasMemoriaDinamica {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // Persona1 --> Persona2 --> Persona3 --> Persona4 --> Persona1
        //          <--

        Jugador j1 = new Jugador("Allan", 0);
        Jugador j2 = new Jugador("Pedro", 0);
        Jugador j3 = new Jugador("Maria", 0);
        Jugador j4 = new Jugador("Ronald", 0);
        Jugador j5 = new Jugador("Chaca", 0);

        Lista jugadores = new Lista();
        jugadores.agregar(j1);
        jugadores.agregar(j2);
        jugadores.agregar(j3);
        jugadores.agregar(j4);
        jugadores.agregar(j5);

        System.out.println(jugadores);
        System.out.println(jugadores.size());
        jugadores.insertar(2, new Jugador("Luisa", 12));

        System.out.println(jugadores);
        System.out.println(jugadores.size());

        jugadores.eliminar(1);
        System.out.println(jugadores);
        System.out.println(jugadores.size());

        jugadores.eliminar(new Jugador("Ronald", 0));
        System.out.println(jugadores);
        System.out.println(jugadores.size());

        System.out.println(jugadores.buscar(new Jugador("Maria",1)));
        System.out.println(jugadores.buscar(1));
        
//        LinkedList<Jugador> jugadores = new LinkedList<>();
//        
//        //Colas
//        jugadores.add(j1); Listo
//        jugadores.add(0, j1);
//        jugadores.addFirst(j5); Listo
//        jugadores.addLast(j5);
//        jugadores.getFirst();
//        jugadores.getLast();
//        jugadores.get(0);
//        jugadores.remove(j5);
//        jugadores.remove(1);
        //Pilas
//        jugadores.push(j5);
//        jugadores.pop();
        //Quien el primero 
        //Cuantos elementos tengo
        //Quién es el último elemento
        //En que posición está Maria
        // Listas Enlazadas/Simples
        // Listas Doblemente Enlazadas
        // Listas Circulares
        // Listas Doblemente Enlazada Circulares
    }

}
