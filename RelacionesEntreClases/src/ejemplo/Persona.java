/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo;

/**
 *
 * @author allanmual
 */
public class Persona {
    
    private String nombre; 
    //La persona tiene una realación de composición con el corazon. 
    private Corazon corazon;//Tiene que creearse dentro de la persona
    private Coche coche;//Relación de asociación
    
    
    public Persona(String nombre) {
        this.nombre = nombre;
        corazon = new Corazon();
    }

    public void asignaCoche(Coche coche) {
        this.coche = coche;
    }
    
    public void viaja(){
    }
    
    public void emociona(){
    }
    
    public void tranquiliza(){
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", corazon=" + corazon + ", coche=" + coche + '}';
    }

    

  
}
