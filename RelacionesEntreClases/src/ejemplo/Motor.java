/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo;

/**
 *
 * @author allanmual
 */
public class Motor {

    //Atributos
    private int revolucionesPorMinuto;
    private boolean activo;
    
    //Constructores
    
    
    //Getters and Setters
    public boolean estaActivo() {
        return activo;
    }
    
    public void cambiaRevolucionesPorMinuto(int revolucionesPorMinuto){
        this.revolucionesPorMinuto = revolucionesPorMinuto;
    }

   
    public void activa(){
        activo = true;
        revolucionesPorMinuto = 1000;
    }
    
    public void desactiva(){
        activo = false;
        revolucionesPorMinuto = 0;
    }
     
    @Override
    public String toString() {
        return "Motor{" + "revolucionesPorMinuto=" + revolucionesPorMinuto + ", activo=" + activo + '}';
    }
    
}
