/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo;

/**
 *
 * @author allanmual
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Ejmplo Persona - Coche");
       
        Persona p1 = new Persona("Allan");
        Motor m1 = new Motor();
        Coche c1 = new Coche(m1);
        p1.asignaCoche(c1);
        System.out.println(p1);
        
        Motor m2 = new Motor();
        Coche c2 = new Coche(m2);
        Persona p2 = new Persona("Juan");
        c2.asignaConductor(p2);
        System.out.println(c2);   
    }
}
