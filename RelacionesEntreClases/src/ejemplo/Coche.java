/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo;

/**
 *
 * @author allanmual
 */
public class Coche {

    private Motor motor; //El coche tiene una relación de agregación con el motor
    private Persona conductor;

    public Coche(Motor motor) {
        this.motor = motor;
    }

    public void asignaConductor(Persona conductor) {
        this.conductor = conductor;
    }
    
    

    public void enciende() {

    }

    public void apaga() {

    }

    public void acelera() {

    }

    public void frena() {

    }

    public boolean estaEncendido() {
        return false;
    }

    @Override
    public String toString() {
        return "Coche{" + "motor=" + motor + ", conductor=" + conductor + '}';
    }

    
}
