/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionesentreclases;

import java.util.Date;

/**
 *
 * @author allanmual
 */
public class RelacionesEntreClases {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Cliente cliente = new Cliente("Allan", "Murillo Alfaro", "50m este de la escuela", "Chachagua", new Date());
        Cliente cliente2 = new Cliente("Roberto", "Murillo Alfaro", "50m este de la escuela", "Bosque", new Date());
        Cuenta cuenta = new Cuenta(12312, 5000, 0.01, cliente);
        cuenta = new Cuenta(12312, 5000, 0.01, cliente);

        System.out.println(cuenta.leerSaldo());
        System.out.println(cuenta.leerTitular().nombreCompleto());
        cuenta.setTitular(cliente2);
        System.out.println(cuenta.leerSaldo());
        System.out.println(cuenta.leerTitular().nombreCompleto());

    }

}
