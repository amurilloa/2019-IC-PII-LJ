/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico;

/**
 *
 * @author allanmual
 */
public class Logica {

    private char[] frase;

    public Logica(String texto) {
        //Manual
//        frase = new char[texto.length()];
//        for (int i = 0; i < frase.length; i++) {
//            frase[i] = texto.charAt(i);
//        }

        //Automatizado
        frase = texto.toCharArray();
    }

    public String imprimir() {
        String res = "";

        for (char c : frase) {
            res += c + "|";
        }

        return res + "\b";
    }

    /**
     * Genera una matriz donde la fila 1 es un arreglo de letras de la frase y 
     * la fila 2 las letras vocales intercabiadas por una x y las consonantes 
     * por una o 
     * @return matriz de letras
     */
    public char[][] generarMatriz() {
        char[] nuevo = new char[frase.length];

        for (int i = 0; i < frase.length; i++) {
            if ("aeiou".contains(String.valueOf(frase[i]).toLowerCase())) {
                nuevo[i] = 'x';
            } else if (Character.isAlphabetic(frase[i])) {
                nuevo[i] = 'o';
            } else {
                nuevo[i] = frase[i];
            }
        }

//        char[][] matriz = new char[2][frase.length];
//        for (int i = 0; i < frase.length; i++) {
//            matriz[0][i] = frase[i];
//            matriz[1][i] = nuevo[i];
//        }
//      return matriz;

        return   new char[][]{frase, nuevo};

    }

    
    public String imprimirM(char[][] matriz) {
        String res = "";
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                res += matriz[i][j] + "|";
            }
            res += "\b\n";
        }
        return res;
    }
}
