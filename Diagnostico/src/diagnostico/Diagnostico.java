/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico;

import javax.swing.JOptionPane;

/**
 *
 * @author allanmual
 */
public class Diagnostico {

    public static String leerFrase(String texto) {
        return JOptionPane.showInputDialog(texto);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String frase = leerFrase("Digite una frase");
        Logica log = new Logica(frase);
        //System.out.println(log.imprimir());
        System.out.println(log.imprimirM(log.generarMatriz()));
    }

}
