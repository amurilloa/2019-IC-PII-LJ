/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectolj.bol;

import demoproyectolj.dao.InsumoDAO;
import demoproyectolj.entities.Insumo;
import demoproyectolj.entities.InsumoServicio;
import demoproyectolj.entities.Servicio;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class InsumoBOL {

    public LinkedList<InsumoServicio> cargarInsumosReq(String filtro, boolean todos, Servicio servicio) {
        validar(filtro, todos);
        return new InsumoDAO().cargarReq(filtro, todos, servicio.getId());
    }

    public LinkedList<Insumo> cargarInsumosDis(String filtro, boolean todos, Servicio servicio) {
        validar(filtro, todos);
        return new InsumoDAO().cargarDis(filtro, todos, servicio.getId());
    }

    private void validar(String filtro, boolean todos) {
        if (!todos && filtro.trim().length() < 2) {
            throw new RuntimeException("Mínimo 2 caracteres para filtrar");
        }
    }

}
