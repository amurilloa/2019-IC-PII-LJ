/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectolj.bol;

import demoproyectolj.dao.ServicioDAO;
import demoproyectolj.entities.Servicio;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class ServicioBOL {

    public LinkedList<Servicio> buscar(String filtro, boolean todos, boolean inactivos) {
        if (filtro.trim().length() < 3 && !todos) {
            throw new RuntimeException("Mínimo 3 caracteres para filtrar");
        }
        return new ServicioDAO().cargarTodo(filtro, todos, inactivos);
    }

    public void eliminar(Servicio servicio) {
        if (servicio == null || servicio.getId() <= 0) {
            throw new RuntimeException("Favor seleccionar el servicio");
        }
        new ServicioDAO().elminar(servicio);
    }

    public void guardar(Servicio servicio) {
        validar(servicio);
        if (servicio.getId() > 0) {
            new ServicioDAO().actualizar(servicio);
        } else {
            new ServicioDAO().insertar(servicio);
        }
    }

    private void validar(Servicio servicio) {
        if (servicio == null) {
            throw new RuntimeException("Favor seleccionar el servicio");
        }

        if (servicio.getCodigo() == null || servicio.getCodigo().trim().isEmpty()) {
            throw new RuntimeException("Código requerido");

        }

        if (servicio.getServicio() == null || servicio.getServicio().trim().isEmpty()) {
            throw new RuntimeException("Código requerido");

        }

        if (servicio.getPrecio() < 0) {
            throw new RuntimeException("Precio no puede ser negativo");
        }
    }

}
