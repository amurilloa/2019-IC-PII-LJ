/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectolj.gui;

import demoproyectolj.entities.MiError;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author allanmual
 */
public class Util {

    public static void mostrarMensaje(JLabel label, String mensaje, int codigo) {
        label.setText(mensaje);
        label.setForeground(codigo == 1 ? Color.RED : codigo == 2 ? Color.GREEN : Color.BLACK);
        label.repaint();
    }

    public static void mostrarMensaje(JLabel label, MiError error) {
        label.setText(error.getMensaje());
        int codigo = error.getCodigo();
        label.setForeground(codigo == 1 ? Color.RED : codigo == 2 ? Color.GREEN : Color.BLACK);
        label.repaint();
    }

    public static void mostrarMensaje(JLabel lbl) {
        lbl.setText("Acción realiza con éxito");
        lbl.setForeground(new Color(0, 148, 0));
    }

}
