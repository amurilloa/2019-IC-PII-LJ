/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectolj.dao;

import demoproyectolj.entities.MiError;
import demoproyectolj.entities.Servicio;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class ServicioDAO {

    public LinkedList<Servicio> cargarTodo(String filtro, boolean todos, boolean inactivos) {
        LinkedList<Servicio> servicios = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            String sql = "select id, codigo, servicio, descripcion, precio, repetitivo, activo "
                    + " from servicios ";

            sql += !todos ? " where (lower(codigo) like lower(?) or lower(servicio) like lower(?))" : "";
            sql += !todos && !inactivos ? " and activo = true " : "";
            sql += todos && !inactivos ? " where activo = true" : "";

            PreparedStatement ps = con.prepareStatement(sql);
            if (!todos) {
                ps.setString(1, "%" + filtro + "%");
                ps.setString(2, "%" + filtro + "%");
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                servicios.add(cargarServicio(rs));
            }

        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
        return servicios;
    }

    private Servicio cargarServicio(ResultSet rs) throws SQLException {
        Servicio ser = new Servicio();
        ser.setId(rs.getInt("id"));
        ser.setCodigo(rs.getString("codigo"));
        ser.setServicio(rs.getString("servicio"));
        ser.setDescripcion(rs.getString("descripcion"));
        ser.setPrecio(rs.getDouble("precio"));
        ser.setRepetitivo(rs.getBoolean("repetitivo"));
        ser.setActivo(rs.getBoolean("activo"));
        return ser;
    }

    public void actualizar(Servicio servicio) {
        try (Connection con = Conexion.conexion()) {
            String sql = "UPDATE servicios SET codigo=?, servicio=?, descripcion=?, precio=?, repetitivo=?, activo=? WHERE id = ?";

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, servicio.getCodigo());
            ps.setString(2, servicio.getServicio());
            ps.setString(3, servicio.getDescripcion());
            ps.setDouble(4, servicio.getPrecio());
            ps.setBoolean(5, servicio.isRepetitivo());
            ps.setBoolean(6, servicio.isActivo());
            ps.setInt(7, servicio.getId());
            ps.executeUpdate();

        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    public void insertar(Servicio servicio) {
        try (Connection con = Conexion.conexion()) {
            String sql = "INSERT INTO servicios(codigo, servicio, "
                    + " descripcion, precio, repetitivo, activo) "
                    + " VALUES (?, ?, ?, ?, ?, ?)";

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, servicio.getCodigo());
            ps.setString(2, servicio.getServicio());
            ps.setString(3, servicio.getDescripcion());
            ps.setDouble(4, servicio.getPrecio());
            ps.setBoolean(5, servicio.isRepetitivo());
            ps.setBoolean(6, servicio.isActivo());
            ps.executeUpdate();

        } catch (Exception e) {
            throw new MiError(e.getMessage());
        }
    }

    public void elminar(Servicio servicio) {
        try (Connection con = Conexion.conexion()) {
            String sql = "UPDATE servicios SET activo=false WHERE id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, servicio.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

}
