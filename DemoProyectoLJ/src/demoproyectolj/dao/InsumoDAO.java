/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectolj.dao;

import demoproyectolj.entities.Insumo;
import demoproyectolj.entities.InsumoServicio;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class InsumoDAO {

    public LinkedList<InsumoServicio> cargarReq(String filtro, boolean todos, int id) {
        LinkedList<InsumoServicio> insumos = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            String sql = "select i.id, i.codigo, i.insumo, i.precio, i.cantidad, i.activo, si.id idSI, si.cantidad canSI"
                    + " from ser_ins si join insumos i on(si.id_insumos = i.id) where si.id_servicios = ?";
            sql += !todos ? " and (lower(i.codigo) like lower(?) or lower(i.insumo) like lower(?))" : "";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            if (!todos) {
                ps.setString(2, "%" + filtro + "%");
                ps.setString(3, "%" + filtro + "%");
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                insumos.add(cargarInsumoServicio(rs));
            }

        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
        return insumos;
    }

    public LinkedList<Insumo> cargarDis(String filtro, boolean todos, int id) {
        LinkedList<Insumo> insumos = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            String sql = "select  i.id, i.codigo, i.insumo, i.precio, i.cantidad, i.activo "
                    + " from insumos i where i.id "
                    + " not in (select id_insumos from ser_ins where id_servicios = ?)";

            sql += !todos ? " and (lower(i.codigo) like lower(?) or lower(i.insumo) like lower(?))" : "";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            if (!todos) {
                ps.setString(2, "%" + filtro + "%");
                ps.setString(3, "%" + filtro + "%");
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                insumos.add(cargarInsumo(rs));
            }

        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
        return insumos;
    }

    private Insumo cargarInsumo(ResultSet rs) throws SQLException {
        Insumo ins = new Insumo();
        ins.setId(rs.getInt("id"));
        ins.setCodigo(rs.getString("codigo"));
        ins.setInsumo(rs.getString("insumo"));
        ins.setPrecio(rs.getDouble("precio"));
        ins.setCantidad(rs.getInt("cantidad"));
        ins.setActivo(rs.getBoolean("activo"));
        return ins;
    }

    private InsumoServicio cargarInsumoServicio(ResultSet rs) throws SQLException {
        InsumoServicio ins = new InsumoServicio();
        ins.setId(rs.getInt("id"));
        ins.setCodigo(rs.getString("codigo"));
        ins.setInsumo(rs.getString("insumo"));
        ins.setPrecio(rs.getDouble("precio"));
        ins.setCantidad(rs.getInt("cantidad"));
        ins.setActivo(rs.getBoolean("activo"));
        ins.setIdSI(rs.getInt("idSI"));
        ins.setCantidadSI(rs.getInt("canSI"));
        return ins;
    }

}
