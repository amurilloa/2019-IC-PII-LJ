/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectolj.entities;

/**
 *
 * @author allanmual
 */
public class InsumoServicio extends Insumo {

    private int idSI;
    private int cantidadSI;

    public InsumoServicio() {
    }

    public InsumoServicio(int idSI, int cantidadSI) {
        this.idSI = idSI;
        this.cantidadSI = cantidadSI;
    }

    public int getIdSI() {
        return idSI;
    }

    public void setIdSI(int idSI) {
        this.idSI = idSI;
    }

    public int getCantidadSI() {
        return cantidadSI;
    }

    public void setCantidadSI(int cantidadSI) {
        this.cantidadSI = cantidadSI;
    }

    @Override
    public String toString() {
        return codigo + "-" + insumo + "(" + cantidadSI + ", " + (activo ? "\u2713" : "X") + (cantidad > cantidadSI ? "\u2713" : "X") + ")";
    }

}
