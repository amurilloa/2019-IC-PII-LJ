/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectolj.entities;

/**
 *
 * @author allanmual
 */
public class MiError extends RuntimeException {

    private int codigo;
    private String mensaje;

    public MiError(String msj) {
        proMensaje(msj);
    }

    private void proMensaje(String msj) {
        if (msj.contains("unq_ser_cod")) {
            mensaje = "Código de servicio ya esta registrado";
            codigo = 1;
        } else {
            mensaje = "Error!!";
        }
    }

    public String getMensaje() {
        return mensaje;
    }

    public int getCodigo() {
        return codigo;
    }

}
