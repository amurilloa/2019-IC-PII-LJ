/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacioncapas.gui;

import java.awt.Component;
import java.awt.Container;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import programacioncapas.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class Util {

    /**
     * Limpia los componentes gráficos de una ventana
     *
     * @param contenedor
     */
    public static void limpiar(Container contenedor) {
        for (int i = 0; i < contenedor.getComponentCount(); i++) {
            if (contenedor.getComponent(i) instanceof JTextField) {
                ((JTextField) contenedor.getComponent(i)).setText("");
            } else if (contenedor.getComponent(i) instanceof JCheckBox) {
                ((JCheckBox) contenedor.getComponent(i)).setSelected(false);
            } else if (contenedor.getComponent(i) instanceof JPanel) {
                limpiar((JPanel) contenedor.getComponent(i));
            }
        }
    }
    
     public static void asignarPermisos(Usuario logeado, JComponent component) {
        if (component instanceof JMenu) {
            for (Component menuComponent : ((JMenu) component).getMenuComponents()) {
                asignarPermisos(logeado, (JComponent) menuComponent);
            }
            if (component.getName() != null) {
                component.setVisible(logeado.permiso(component.getName()));
            }
        } else if (component instanceof JMenuItem) {
            if (component.getName() != null) {
                component.setVisible(logeado.permiso(component.getName()));
            }
        }
    }
}
