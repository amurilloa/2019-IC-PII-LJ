/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacioncapas.gui;

import programacioncapas.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class Main {

    public static void main(String[] args) {
        //GUI --> BOL --> DAL
        //     Entities

        //Entities: Clases
        //GUI: Formularios(JFrame, JDialog...)
        //BOL: Métodos, Validaciones, Logica de Negocios
        //DAL: Lectura y escritura de datos(Base Datos, Archivos)
        
      
        new FrmLogin().setVisible(true);
    }
}
