/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacioncapas.bol;

import java.util.LinkedList;
import programacioncapas.dao.AccesoDAO;
import programacioncapas.entities.Acceso;
import programacioncapas.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class AccesoBOL {

    public LinkedList<Acceso> cargarTodos() {
        return new AccesoDAO().cargarTodos();
    }

    public void agregarAcceso(Acceso accSel, Usuario usuario) {
        if (accSel == null || accSel.getId().isEmpty()) {
            throw new RuntimeException("Favor seleccionar un acceso disponible");
        }
        if (usuario == null || usuario.getUsuario().isEmpty()) {
            throw new RuntimeException("Favor seleccionar un usuario");
        }
        
        new AccesoDAO().insertar(accSel, usuario);
    }

}
