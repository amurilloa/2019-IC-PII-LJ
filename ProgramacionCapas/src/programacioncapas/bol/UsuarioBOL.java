/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacioncapas.bol;

import java.util.LinkedList;
import programacioncapas.dao.UsuarioDAO;
import programacioncapas.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class UsuarioBOL {

    private final int MIN_PASS = 8;

    /**
     *
     * @param u
     * @param repass
     * @return
     */
    public boolean registrar(Usuario u, String repass) {
        //Validaciones
         if (u.getContrasenna().isEmpty() || !u.getContrasenna().equals(repass)) {
            throw new RuntimeException("Contraseña inválida o no coinciden");
        }

        if (u.getContrasenna().length() < MIN_PASS) {
            throw new RuntimeException("Tamaño mínimo de contraseña 8 caracteres");
        }

        if (u.getNombre().isEmpty()) {
            throw new RuntimeException("Nombre requerido");
        }

        if (u.getApellidoUno().isEmpty()) {
            throw new RuntimeException("Primer apellido requerido");
        }

        if (u.getCorreo().isEmpty()) {
            throw new RuntimeException("Correo requerido");
        }

        //TODO: Formato del correo...
        if (u.getUsuario().isEmpty()) {
            throw new RuntimeException("Usuario requerido");
        }

        UsuarioDAO uda = new UsuarioDAO();
        uda.insertar(u);
        return true;
    }

    private void validar(Usuario u, String repass) {
        if (u.getContrasenna().isEmpty() || !u.getContrasenna().equals(repass)) {
            throw new RuntimeException("Contraseña inválida o no coinciden");
        }

        if (u.getContrasenna().length() < MIN_PASS) {
            throw new RuntimeException("Tamaño mínimo de contraseña 8 caracteres");
        }

        if (u.getNombre().isEmpty()) {
            throw new RuntimeException("Nombre requerido");
        }

        if (u.getApellidoUno().isEmpty()) {
            throw new RuntimeException("Primer apellido requerido");
        }

        if (u.getCorreo().isEmpty()) {
            throw new RuntimeException("Correo requerido");
        }

        //TODO: Formato del correo...
        if (u.getUsuario().isEmpty()) {
            throw new RuntimeException("Usuario requerido");
        }
    }

    public void login(Usuario u) {

        if ((u.getUsuario() == null || u.getUsuario().isEmpty())
                && u.getCorreo() == null || u.getCorreo().isEmpty()) {
            throw new RuntimeException("Usuario o contraseña inválidos");
        }

        if (u.getContrasenna().length() < MIN_PASS) {
            throw new RuntimeException("Usuario o contraseña inválidos");
        }
        
        new UsuarioDAO().autenticar(u);
    }

    public LinkedList<Usuario> cargarUsuarios(String filtro) {
        return new UsuarioDAO().cargarUsuarios(filtro);
    }

}
