/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacioncapas.dao;

import java.util.LinkedList;
import programacioncapas.entities.Acceso;
import programacioncapas.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class AccesoDAO {

    private final String RUTA_PER = "datos/permisos.txt";
    private final String RUTA_USU_PER = "datos/usuario_permiso.txt";

    public LinkedList<Acceso> cargarTodos() {
        LinkedList<Acceso> accesos = new LinkedList<>();

        try {
            ManejoArchivos ma = new ManejoArchivos();
            String[] datos = ma.leer(RUTA_PER).split("\n");
            for (String dato : datos) {
                String[] temp = dato.split(",");
                accesos.add(cargarAcceso(temp));
            }
        } catch (Exception e) {
            throw new RuntimeException("No hay permisos registrados");
        }

        return accesos;
    }

    private Acceso cargarAcceso(String[] temp) {
        Acceso a = new Acceso();
        a.setId(temp[0]);
        a.setDesc(temp[1]);
        return a;
    }

    public void insertar(Acceso accSel, Usuario usuario) {
        try {
            ManejoArchivos ma = new ManejoArchivos();
            String leer = ma.leer(RUTA_USU_PER);
            String data = String.format("%s,%s", usuario.getUsuario(), accSel.getId());
            ma.escribir(RUTA_USU_PER, leer + data);
        } catch (Exception e) {
            throw new RuntimeException("Problemas al almacenar los usuarios");
        }
    }

    LinkedList<Acceso> cargar(Usuario u) {
        LinkedList<Acceso> accesos = new LinkedList<>();
        try {
            ManejoArchivos ma = new ManejoArchivos();
            String[] datos = ma.leer(RUTA_USU_PER).split("\n");
            for (String dato : datos) {
                String[] temp = dato.split(",");
                if (u.getUsuario().equals(temp[0])) {
                    Acceso a = new Acceso();
                    a.setId(temp[1]);
                    accesos.add(a);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("No hay permisos registrados");
        }
        return accesos;
    }

}
