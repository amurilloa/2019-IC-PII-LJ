/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacioncapas.dao;

import java.io.IOException;
import java.util.LinkedList;
import programacioncapas.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class UsuarioDAO {

    private final String RUTA_ARC = "datos/usuarios.txt";

    /**
     *
     * @param u
     */
    public void insertar(Usuario u) {
        try {
            ManejoArchivos ma = new ManejoArchivos();
            String leer = ma.leer(RUTA_ARC);
            if (leer.toLowerCase().contains(u.getUsuario().toLowerCase())) {
                throw new RuntimeException("Nombre de usuario ya está "
                        + " siendo utilizado");
            }
            ma.escribir(RUTA_ARC, leer + u.getData());
        } catch (IOException e) {
            throw new RuntimeException("Problemas al almacenar los usuarios");
        } catch (RuntimeException e) {
            throw e;
        }

    }

    public void autenticar(Usuario u) {
        try {
            ManejoArchivos ma = new ManejoArchivos();
            String leer = ma.leer(RUTA_ARC);
            String usuarios[] = leer.split("\n");
            for (String usuario : usuarios) {
                String[] datos = usuario.split(",");
                if ((datos[0].equals(u.getUsuario()) || datos[4].equals(u.getCorreo()))
                        && datos[5].equals(u.getContrasenna())) {
                    cargarUsuario(datos, u);
                    return;
                }
            }
            u.setUsuario(null);
        } catch (Exception e) {
            throw new RuntimeException("Problemas al almacenar los usuarios");
        }

    }

    public LinkedList<Usuario> cargarUsuarios(String filtro) {
        LinkedList<Usuario> users = new LinkedList<>();

        try {
            ManejoArchivos ma = new ManejoArchivos();
            String usuarios[] = ma.leer(RUTA_ARC).split("\n");

            for (String usuario : usuarios) {
                if (usuario.toLowerCase().contains(filtro.toLowerCase())) {
                    String[] datos = usuario.split(",");
                    Usuario u = new Usuario();
                    cargarUsuario(datos, u);
                    users.add(u);
                }
            }
        } catch (Exception ex) {
            throw new RuntimeException("No hay usuarios registrados");
        }

        return users;
    }

    private void cargarUsuario(String[] datos, Usuario u) {
        u.setUsuario(datos[0]);
        u.setNombre(datos[1]);
        u.setApellidoUno(datos[2]);
        u.setApellidoDos(datos[3]);
        u.setCorreo(datos[4]);
        u.setContrasenna("");
        u.setAdmin(Boolean.parseBoolean(datos[6]));
        AccesoDAO adao = new AccesoDAO();
        u.setAccesos(adao.cargar(u));
    }
}
