/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacioncapas.entities;

import java.util.LinkedList;
import java.util.Objects;

/**
 *
 * @author allanmual
 */
public class Usuario {

    private String usuario;
    private String nombre;
    private String apellidoUno;
    private String apellidoDos;
    private String correo;
    private String contrasenna;
    private boolean admin;
    private LinkedList<Acceso> accesos;

    public Usuario() {
        accesos = new LinkedList<>();
    }

    public Usuario(String usuario, String nombre, String apellidoUno, String apellidoDos, String correo, String contrasenna, boolean admin) {
        this.usuario = usuario;
        this.nombre = nombre;
        this.apellidoUno = apellidoUno;
        this.apellidoDos = apellidoDos;
        this.correo = correo;
        this.contrasenna = contrasenna;
        this.admin = admin;
        accesos = new LinkedList<>();
    }

    public boolean permiso(String idPermiso) {
        return accesos.contains(new Acceso(idPermiso));

//        for (Acceso acceso : accesos) {
//            if (acceso.getId().equals(permiso)) {
//                return true;
//            }
//        }
//        return false;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoUno() {
        return apellidoUno;
    }

    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    public String getApellidoDos() {
        return apellidoDos;
    }

    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public LinkedList<Acceso> getAccesos() {
        return accesos;
    }

    public void setAccesos(LinkedList<Acceso> accesos) {
        this.accesos = accesos;
    }

    @Override
    public String toString() {
        return String.format("%s: %s %s (%s)", usuario, nombre, apellidoUno, correo);
    }

    public String getData() {
        return usuario + "," + nombre + "," + apellidoUno + ","
                + apellidoDos + "," + correo + "," + contrasenna + "," + admin;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.usuario, other.usuario)) {
            return false;
        }
        if (!Objects.equals(this.correo, other.correo)) {
            return false;
        }
        return true;
    }
    
    
    

}
