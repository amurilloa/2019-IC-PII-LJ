/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author allanmual
 */
public class Vehiculo {

    private String marca;
    private String modelo;
    private int anno;
    private String color;
    private int precio;

    public Vehiculo() {
    }

    public Vehiculo(String marca, String modelo, int anno, String color, int precio) {
        this.marca = marca;
        this.modelo = modelo;
        this.anno = anno;
        this.precio = precio;
        this.color = color;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getData() {
        return marca + "," + modelo + "," + anno + "," + precio + "," + color;
    }

    @Override
    public String toString() {
        return "Vehiculo{" + "marca=" + marca + ", modelo=" + modelo + ", anno=" + anno + ", precio=" + precio + ", color=" + color + '}';
    }

}
