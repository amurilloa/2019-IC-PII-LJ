/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  allanmual
 * Created: 04-abr-2019
 */

CREATE TABLE usuarios(
	id serial,
	usuario text NOT NULL,
	correo text NOT NULL,
	contrasena text NOT NULL, 
	activo boolean DEFAULT true, 
	CONSTRAINT pk_usuarios PRIMARY KEY(id), 
	CONSTRAINT unq_usu_usu UNIQUE(usuario),
	CONSTRAINT unq_usu_cor UNIQUE(correo)
);

SELECT * FROM usuarios;

CREATE TABLE preferencias(
	id serial,
	preferencia text NOT NULL, 
	activo boolean DEFAULT true,
	CONSTRAINT pk_preferencias PRIMARY KEY(id),
	CONSTRAINT unq_pre_pre UNIQUE(preferencia)
);

INSERT INTO preferencias(preferencia) VALUES 
('Modo'),
('Tiempo');


CREATE TABLE preferencia_opciones(
	id serial,
	id_preferencia int NOT NULL, 
	opcion text NOT NULL, 
	valor int DEFAULT 0,
	valor_min int DEFAULT 0, 
	valor_max int DEFAULT 0,
	CONSTRAINT pk_preopc PRIMARY KEY(id),
	CONSTRAINT fk_preopc_pre FOREIGN KEY(id_preferencia) REFERENCES preferencias(id)
);

INSERT INTO preferencia_opciones(id_preferencia, opcion, valor, valor_min, valor_max) VALUES
(1, 'Presentación', 1, 1, 1), 
(1, 'Mosaico', 2, 2, 2);

INSERT INTO preferencia_opciones(id_preferencia, opcion, valor, valor_min, valor_max) VALUES
(2, 'Rango Tiempo', 4, 1, 20);

CREATE TABLE usuarios_preferencias(
	id serial,
	id_usuario int NOT NULL, 
	id_preferencia int NOT NULL,
	valor int default 0,
	CONSTRAINT pk_usupre PRIMARY KEY(id),
	CONSTRAINT fk_usupre_usu FOREIGN KEY(id_usuario) REFERENCES usuarios(id), 
	CONSTRAINT fk_usupre_pre FOREIGN KEY(id_preferencia) REFERENCES preferencias(id), 
	CONSTRAINT unq_usupre_usupre UNIQUE(id_usuario, id_preferencia)
);

CREATE TABLE archivos(
	id serial, 
	archivo bytea NOT NULL,
	CONSTRAINT pk_archivos PRIMARY KEY(id)
);

CREATE TABLE imagenes(
	id serial,
	id_usuario int NOT NULL,
	id_archivo int NOT NULL,
	titulo text NOT NULL,
	descripcion text NOT NULL,
	fecha date,
	formato text NOT NULL,
	gps text, 
	visible boolean DEFAULT true,
	CONSTRAINT pk_imagenes PRIMARY KEY(id), 
	CONSTRAINT fk_img_usu FOREIGN KEY(id_usuario) REFERENCES usuarios(id),
	CONSTRAINT fk_img_arc FOREIGN KEY(id_archivo) REFERENCES archivos(id)
);

