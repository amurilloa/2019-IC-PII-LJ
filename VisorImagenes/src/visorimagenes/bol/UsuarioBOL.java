/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.bol;

import visorimagenes.entities.Usuario;
import org.apache.commons.codec.digest.DigestUtils;
import visorimagenes.dao.UsuarioDAO;

/**
 *
 * @author allanmual
 */
public class UsuarioBOL {

    public int guardar(Usuario usuario, String recon) {
        validar(usuario, recon);
        usuario.setContrasena(md5(usuario.getContrasena()));
        if (usuario.getId() > 0) {
            return new UsuarioDAO().actualizar(usuario);
        } else {
            return new UsuarioDAO().insertar(usuario);
        }
    }

    private void validar(Usuario usuario, String recon) {
        if (usuario == null) {
            throw new RuntimeException("Usuario inválido");
        }

        if (usuario.getUsuario() == null || usuario.getUsuario().trim().isEmpty()) {
            throw new RuntimeException("Nombre de usuario requerido");
        }

        if (usuario.getContrasena() == null || usuario.getContrasena().length() < 8) {
            throw new RuntimeException("Contraseña debe tener mínimo 8 caracteres");
        }

        if (usuario.getCorreo() == null || usuario.getCorreo().trim().isEmpty()) {
            throw new RuntimeException("Correo requerido");
        }

        if (recon == null || !usuario.getContrasena().equals(recon.trim())) {
            throw new RuntimeException("Las contraseñas no coinciden");
        }
    }

    /**
     *
     * @param input
     * @return Referencia, Librería:
     * http://commons.apache.org/proper/commons-codec/download_codec.cgi
     */
    private String md5(String input) {
        String result = DigestUtils.md5Hex(input);
        return result;
    }

}
