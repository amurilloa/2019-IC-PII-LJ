/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.bol;

import java.util.LinkedList;
import visorimagenes.dao.PreferenciaDAO;
import visorimagenes.entities.Preferencia;
import visorimagenes.entities.PreferenciaOpcion;
import visorimagenes.entities.PreferenciaUsuario;
import visorimagenes.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class PreferenciaBOL {

    public LinkedList<Preferencia> cargar() {
        return new PreferenciaDAO().cargar();
    }

    public boolean guardar(PreferenciaUsuario preUsu, Usuario usuario, PreferenciaOpcion opcion) {
        validar(preUsu, usuario, opcion);
        return new PreferenciaDAO().insertar(preUsu.getId(), usuario.getId(), preUsu.getValor());
    }

    private void validar(PreferenciaUsuario preUsu, Usuario usuario, PreferenciaOpcion opcion) {
        if (preUsu == null || usuario == null) {
            throw new RuntimeException("Datos inválidos");
        }
        if (opcion != null && (preUsu.getValor() < opcion.getValorMin() || preUsu.getValor() > opcion.getValorMax())) {
            throw new RuntimeException("Valor fuera de los límites establecidos");
        }
        if (usuario.getId() <= 0) {
            throw new RuntimeException("Usuario inválido");
        }

        if (preUsu.getId() <= 0) {
            throw new RuntimeException("Preferencia inválida");
        }
    }

}
