/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.entities;

import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Preferencia {

    protected int id;
    protected String preferencia;
    protected boolean activo;
    protected LinkedList<PreferenciaOpcion> opciones;

    public Preferencia() {
        opciones = new LinkedList<>();
    }

    public Preferencia(int id, String preferencia, boolean activo) {
        this.id = id;
        this.preferencia = preferencia;
        this.activo = activo;
        opciones = new LinkedList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPreferencia() {
        return preferencia;
    }

    public void setPreferencia(String preferencia) {
        this.preferencia = preferencia;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public LinkedList<PreferenciaOpcion> getOpciones() {
        return opciones;
    }

    public void setOpciones(LinkedList<PreferenciaOpcion> opciones) {
        this.opciones = opciones;
    }

    @Override
    public String toString() {
        return preferencia;
    }

}
