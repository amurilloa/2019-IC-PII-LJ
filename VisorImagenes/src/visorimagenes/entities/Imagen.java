/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.entities;

import java.time.LocalDateTime;

/**
 *
 * @author allanmual
 */
public class Imagen {
    
    private int id;
    //private int id_usuario;
    //private int id_archivo;
    private String titulo;
    private String descripcion;
    private LocalDateTime fecha;
    private String formato;
    private String gps;
    private boolean visible;

    public Imagen() {
    }

    public Imagen(int id, String titulo, String descripcion, LocalDateTime fecha, String formato, String gps, boolean visible) {
        this.id = id;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.formato = formato;
        this.gps = gps;
        this.visible = visible;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public String toString() {
        return "Imagen{" + "id=" + id + ", titulo=" + titulo + ", descripcion=" + descripcion + ", fecha=" + fecha + ", formato=" + formato + ", gps=" + gps + ", visible=" + visible + '}';
    }
    
    
    
}
