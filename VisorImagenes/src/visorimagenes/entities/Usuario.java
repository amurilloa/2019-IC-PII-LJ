/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.entities;

import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Usuario {

    private int id;
    private String usuario;
    private String correo;
    private String contrasena;
    private boolean activo;
    private LinkedList<PreferenciaUsuario> preferencias;

    public Usuario() {
        activo = true;
        preferencias = new LinkedList<>();
    }

    public Usuario(int id, String usuario, String correo, String contrasena, boolean activo) {
        this.id = id;
        this.usuario = usuario;
        this.correo = correo;
        this.contrasena = contrasena;
        this.activo = activo;
        preferencias = new LinkedList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public LinkedList<PreferenciaUsuario> getPreferencias() {
        return preferencias;
    }

    public void setPreferencias(LinkedList<PreferenciaUsuario> preferencias) {
        this.preferencias = preferencias;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", usuario=" + usuario + ", correo=" + correo + ", contrasena=" + contrasena + ", activo=" + activo + '}';
    }

}
