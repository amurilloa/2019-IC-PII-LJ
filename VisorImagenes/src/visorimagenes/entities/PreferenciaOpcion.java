/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.entities;

/**
 *
 * @author allanmual
 */
public class PreferenciaOpcion {

    private int id;
    private String opcion;
    private int valor;
    private int valorMin;
    private int valorMax;

    public PreferenciaOpcion() {
    }

    public PreferenciaOpcion(int id, String opcion, int valor, int valorMin, int valorMax) {
        this.id = id;
        this.opcion = opcion;
        this.valor = valor;
        this.valorMin = valorMin;
        this.valorMax = valorMax;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOpcion() {
        return opcion;
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getValorMin() {
        return valorMin;
    }

    public void setValorMin(int valorMin) {
        this.valorMin = valorMin;
    }

    public int getValorMax() {
        return valorMax;
    }

    public void setValorMax(int valorMax) {
        this.valorMax = valorMax;
    }

    @Override
    public String toString() {
        return opcion;
    }
    
}
