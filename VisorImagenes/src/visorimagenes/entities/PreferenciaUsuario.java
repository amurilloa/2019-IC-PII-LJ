/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.entities;

/**
 *
 * @author allanmual
 */
public class PreferenciaUsuario extends Preferencia {
    
    private int idUsuPre;
    private int valor;

    public PreferenciaUsuario() {
    }

    public PreferenciaUsuario(int idUsuPre, int valor) {
        this.idUsuPre = idUsuPre;
        this.valor = valor;
    }

    public PreferenciaUsuario(int idUsuPre, int valor, int id, String preferencia, boolean activo) {
        super(id, preferencia, activo);
        this.idUsuPre = idUsuPre;
        this.valor = valor;
    }

    public int getIdUsuPre() {
        return idUsuPre;
    }

    public void setIdUsuPre(int idUsuPre) {
        this.idUsuPre = idUsuPre;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "PreferenciaUsuario{" + "idUsuPre=" + idUsuPre + ", valor=" + valor + '}';
    }

}
