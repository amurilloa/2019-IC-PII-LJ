/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.gui;

import java.awt.Component;
import java.util.LinkedList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import visorimagenes.bol.PreferenciaBOL;
import visorimagenes.bol.UsuarioBOL;
import visorimagenes.entities.Preferencia;
import visorimagenes.entities.PreferenciaOpcion;
import visorimagenes.entities.PreferenciaUsuario;
import visorimagenes.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class FrmUsuario extends javax.swing.JFrame {

    private Usuario usuario;
    private UsuarioBOL ubo;
    private PreferenciaBOL pbo;

    /**
     * Creates new form FrmUsuario
     */
    public FrmUsuario() {
        initComponents();
        setLocationRelativeTo(null);
        ubo = new UsuarioBOL();
        pbo = new PreferenciaBOL();
        usuario = new Usuario();
        cargar();
        cargarPre();
    }

    public FrmUsuario(Usuario usuario) {
        initComponents();
        setLocationRelativeTo(null);
        ubo = new UsuarioBOL();
        pbo = new PreferenciaBOL();
        this.usuario = usuario;
        cargar();
        cargarPre();
    }

    private void guardar() {
        try {
            if (usuario == null) {
                usuario = new Usuario();
            }
            String con = new String(txtContrasena.getPassword()).trim();
            String recon = new String(txtReContrasena.getPassword()).trim();
            usuario.setUsuario(txtUsuario.getText().trim());
            usuario.setCorreo(txtCorreo.getText().trim());
            usuario.setContrasena(con);

            usuario.setId(ubo.guardar(usuario, recon));

            if (usuario.getId() > 0) {
                System.out.println("mensaje de realizado con éxito!!");
                btnEliminar.setEnabled(true);
                cargarPre();
            } else {
                usuario = new Usuario();
                cargar();
            }
        } catch (Exception e) {
            //TODO: Controlar el error
            System.out.println("Error: " + e.getMessage());
        }
    }

    private void guardarPreferencia() {
        try {
            PreferenciaUsuario pu = new PreferenciaUsuario();
            Preferencia sel = (Preferencia) cbxPreferencias.getSelectedItem();
            PreferenciaOpcion opSel = (PreferenciaOpcion) cbxOpciones.getSelectedItem();

            pu.setId(sel.getId());
            pu.setValor((int) spnValor.getValue());

            if (pbo.guardar(pu, usuario, opSel)) {
                //Refrescar los datos
            }
        } catch (Exception e) {
            //TODO: Controlar el error
            System.out.println("Error: " + e.getMessage());

        }
    }

    private void cargarPre() {
        try {
            LinkedList<Preferencia> preferencias = pbo.cargar();
            Preferencia[] elementos = preferencias.toArray(new Preferencia[preferencias.size()]);
            cbxPreferencias.setModel(new DefaultComboBoxModel<>(elementos));
            cargarOpcion();
        } catch (Exception e) {
            //TODO: Controlar el error
            System.out.println("Error: " + e.getMessage());
        }
    }

    private void cargarOpcion() {
        try {
            Preferencia sel = (Preferencia) cbxPreferencias.getSelectedItem();
            if (sel != null) {
                PreferenciaOpcion[] elementos = sel.getOpciones().toArray(new PreferenciaOpcion[sel.getOpciones().size()]);
                cbxOpciones.setModel(new DefaultComboBoxModel<>(elementos));
                PreferenciaOpcion temp = (PreferenciaOpcion) cbxOpciones.getSelectedItem();
                spnValor.setValue(temp.getValor());
                cbxOpciones.setVisible(elementos.length != 0);
                spnValor.setVisible(elementos.length == 0);
                if (elementos.length == 1) {
                    spnValor.setVisible(true);
                    spnValor.setModel(new javax.swing.SpinnerNumberModel(temp.getValor(),
                            temp.getValorMin(), temp.getValorMax(), 1));
                }

            }
        } catch (Exception e) {
            //TODO: Controlar el error
            System.out.println("Error: " + e.getMessage());
        }

    }

    private void seleccionarOpcion() {
        PreferenciaOpcion temp = (PreferenciaOpcion) cbxOpciones.getSelectedItem();
        if (temp != null) {
            spnValor.setValue(temp.getValor());
        }
    }

    private void eliminar() {
        try {
            String recon = new String(txtReContrasena.getPassword()).trim();
            usuario.setActivo(false);
            ubo.guardar(usuario, recon);

        } catch (Exception e) {
            //TODO: Controlar el error
            System.out.println("Error: " + e.getMessage());
        }
    }

    private void cargar() {
        txtUsuario.setText(usuario.getUsuario());
        txtCorreo.setText(usuario.getCorreo());
        txtContrasena.setText("");
        txtReContrasena.setText("");
        btnEliminar.setEnabled(usuario.getId() > 0);
    }

    private void habilitarPreferencias(boolean opcion) {
        for (Component component : pnlPreferencias.getComponents()) {
            component.setEnabled(opcion);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel3 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtCorreo = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtContrasena = new javax.swing.JPasswordField();
        jLabel9 = new javax.swing.JLabel();
        txtReContrasena = new javax.swing.JPasswordField();
        btnGuardar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        pnlPreferencias = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listaPreferencias = new javax.swing.JList<>();
        cbxPreferencias = new javax.swing.JComboBox<>();
        cbxOpciones = new javax.swing.JComboBox<>();
        btnGuardarPre = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        spnValor = new javax.swing.JSpinner();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Usuarios - Visor Imágenes UTN v.01");
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(455, 534));
        setSize(new java.awt.Dimension(455, 534));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Registro de Usuario");

        jSeparator1.setForeground(new java.awt.Color(153, 153, 153));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Datos del Usuario", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 0, 18))); // NOI18N

        jLabel6.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel6.setText("Usuario:");

        txtUsuario.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        txtUsuario.setNextFocusableComponent(txtCorreo);

        jLabel7.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel7.setText("Correo:");

        txtCorreo.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        txtCorreo.setNextFocusableComponent(txtContrasena);

        jLabel8.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel8.setText("Contraseña:");

        txtContrasena.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        txtContrasena.setNextFocusableComponent(txtReContrasena);

        jLabel9.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel9.setText("Re-contraseña:");

        txtReContrasena.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        txtReContrasena.setNextFocusableComponent(btnGuardar);

        btnGuardar.setBackground(new java.awt.Color(204, 255, 204));
        btnGuardar.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.setNextFocusableComponent(txtUsuario);
        btnGuardar.setPreferredSize(new java.awt.Dimension(113, 54));
        btnGuardar.setSize(new java.awt.Dimension(113, 54));
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnEliminar.setBackground(new java.awt.Color(255, 204, 204));
        btnEliminar.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.setPreferredSize(new java.awt.Dimension(113, 54));
        btnEliminar.setSize(new java.awt.Dimension(113, 54));
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtCorreo, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtReContrasena, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtContrasena, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(35, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtContrasena, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtReContrasena, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlPreferencias.setBackground(new java.awt.Color(255, 255, 255));
        pnlPreferencias.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Preferencias del Usuario", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 0, 18))); // NOI18N

        listaPreferencias.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jScrollPane1.setViewportView(listaPreferencias);

        cbxPreferencias.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        cbxPreferencias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxPreferenciasActionPerformed(evt);
            }
        });

        cbxOpciones.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        cbxOpciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxOpcionesActionPerformed(evt);
            }
        });

        btnGuardarPre.setBackground(new java.awt.Color(204, 255, 204));
        btnGuardarPre.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        btnGuardarPre.setText("Guardar");
        btnGuardarPre.setNextFocusableComponent(txtUsuario);
        btnGuardarPre.setPreferredSize(new java.awt.Dimension(113, 54));
        btnGuardarPre.setSize(new java.awt.Dimension(113, 54));
        btnGuardarPre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarPreActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel2.setText("Seleccione una opción:");

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel3.setText("Valor: ");

        spnValor.setModel(new javax.swing.SpinnerNumberModel(0, 0, 1, 1));

        javax.swing.GroupLayout pnlPreferenciasLayout = new javax.swing.GroupLayout(pnlPreferencias);
        pnlPreferencias.setLayout(pnlPreferenciasLayout);
        pnlPreferenciasLayout.setHorizontalGroup(
            pnlPreferenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPreferenciasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlPreferenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                    .addComponent(cbxPreferencias, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbxOpciones, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlPreferenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnGuardarPre, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                    .addComponent(spnValor))
                .addContainerGap())
        );
        pnlPreferenciasLayout.setVerticalGroup(
            pnlPreferenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPreferenciasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlPreferenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlPreferenciasLayout.createSequentialGroup()
                        .addGroup(pnlPreferenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlPreferenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbxPreferencias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(spnValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlPreferenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnGuardarPre, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbxOpciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlPreferencias, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlPreferencias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        eliminar();
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        guardar();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void cbxPreferenciasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxPreferenciasActionPerformed
        cargarOpcion();
    }//GEN-LAST:event_cbxPreferenciasActionPerformed

    private void cbxOpcionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxOpcionesActionPerformed
        seleccionarOpcion();
    }//GEN-LAST:event_cbxOpcionesActionPerformed

    private void btnGuardarPreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarPreActionPerformed
        guardarPreferencia();
    }//GEN-LAST:event_btnGuardarPreActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmUsuario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnGuardarPre;
    private javax.swing.JComboBox<PreferenciaOpcion> cbxOpciones;
    private javax.swing.JComboBox<Preferencia> cbxPreferencias;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JList<String> listaPreferencias;
    private javax.swing.JPanel pnlPreferencias;
    private javax.swing.JSpinner spnValor;
    private javax.swing.JPasswordField txtContrasena;
    private javax.swing.JTextField txtCorreo;
    private javax.swing.JPasswordField txtReContrasena;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
