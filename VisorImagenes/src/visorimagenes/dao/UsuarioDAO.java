/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import visorimagenes.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class UsuarioDAO {

    public int insertar(Usuario usuario) {
        try (Connection con = Conexion.conexion()) {
            //1. Definir el script
            String sql = "INSERT INTO usuarios(usuario, correo, contrasena)"
                    + " VALUES (?, ?, ?) returning id";

            //2. Crear el PreparedStatement con la conexion y el script
            PreparedStatement ps = con.prepareStatement(sql);

            //3. Si tengo parámetros los llenamos y contamos a partir de 1 
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getCorreo());
            ps.setString(3, usuario.getContrasena());

            //4. Se ejecuta y se recupera el id asociado al insert
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }

            return 0;

        } catch (Exception e) {
            String msj = e.getMessage();
            if (msj.contains("unq_")) {
                if (msj.contains("_cor")) {
                    throw new RuntimeException("Correo previamente registrado");
                } else if (msj.contains("_usu")) {
                    throw new RuntimeException("Usuario previamente registrado");
                }
            }
            throw new RuntimeException("Problemas al registrar el usuario, favor intente nuevamente!!");
        }
    }

    public int actualizar(Usuario usuario) {
        try (Connection con = Conexion.conexion()) {
            //1. Definir el script
            String sql = "UPDATE usuarios SET usuario=?, correo=?, contrasena=?, activo=? "
                    + " WHERE id = ?";

            //2. Crear el PreparedStatement con la conexion y el script
            PreparedStatement ps = con.prepareStatement(sql);

            //3. Si tengo parámetros los llenamos y contamos a partir de 1 
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getCorreo());
            ps.setString(3, usuario.getContrasena());
            ps.setBoolean(4, usuario.isActivo());
            ps.setInt(5, usuario.getId());
            
            
            //4. Se ejecuta la consulta si se realizó correctamente, se retorna el mismo id del usuario
            int can = ps.executeUpdate();
            if (can > 0) {
                return usuario.getId();
            }

            return 0;

        } catch (Exception e) {
            String msj = e.getMessage();
            if (msj.contains("unq_")) {
                if (msj.contains("_cor")) {
                    throw new RuntimeException("Correo previamente registrado");
                } else if (msj.contains("_usu")) {
                    throw new RuntimeException("Usuario previamente registrado");
                }
            }
            throw new RuntimeException("Problemas al registrar el usuario, favor intente nuevamente!!");
        }
    }

}
