/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import visorimagenes.entities.Preferencia;
import visorimagenes.entities.PreferenciaOpcion;

/**
 *
 * @author allanmual
 */
public class PreferenciaDAO {

    public LinkedList<Preferencia> cargar() {
        LinkedList<Preferencia> preferencias = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            //1. Definir el script
            String sql = "SELECT id, preferencia, activo FROM preferencias WHERE activo = true";

            //2. Crear el PreparedStatement con la conexion y el script
            PreparedStatement ps = con.prepareStatement(sql);

            //3. Si tengo parámetros los llenamos y contamos a partir de 1 
            //4. Se ejecuta y se recupera el id asociado al insert
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                preferencias.add(cargar(rs));
            }
        } catch (Exception e) {
            throw new RuntimeException("Problemas al cargar las prerefencias");
        }
        return preferencias;
    }

    private Preferencia cargar(ResultSet rs) throws SQLException {
        Preferencia p = new Preferencia();
        p.setId(rs.getInt("id"));
        p.setPreferencia(rs.getString("preferencia"));
        p.setActivo(rs.getBoolean("activo"));
        p.setOpciones(cargarOpciones(p.getId()));
        return p;
    }

    private LinkedList<PreferenciaOpcion> cargarOpciones(int id) {
        LinkedList<PreferenciaOpcion> opciones = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            //1. Definir el script
            String sql = "SELECT id, id_preferencia, opcion, valor, valor_min, valor_max, x"
                    + " FROM preferencia_opciones WHERE id_preferencia = ?";

            //2. Crear el PreparedStatement con la conexion y el script
            PreparedStatement ps = con.prepareStatement(sql);

            //3. Si tengo parámetros los llenamos y contamos a partir de 1 
            ps.setInt(1, id);

            //4. Se ejecuta y se recupera el id asociado al insert
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                opciones.add(cargarOpciones(rs));
            }
        } catch (Exception e) {
            throw new RuntimeException("Problemas al cargar las opciones");
        }
        return opciones;
    }

    private PreferenciaOpcion cargarOpciones(ResultSet rs) throws SQLException {
        PreferenciaOpcion po = new PreferenciaOpcion();
        po.setId(rs.getInt("id"));
        po.setOpcion(rs.getString("opcion"));
        po.setValor(rs.getInt("valor"));
        po.setValorMax(rs.getInt("valor_max"));
        po.setValorMin(rs.getInt("valor_min"));
        return po;
    }

    public boolean insertar(int idPreferencia, int idUsuario, int valor) {
        try (Connection con = Conexion.conexion()) {
            //1. Definir el script
            String sql = "INSERT INTO usuarios_preferencias(id_usuario, id_preferencia, valor) VALUES(?, ?, ?)";

            //2. Crear el PreparedStatement con la conexion y el script
            PreparedStatement ps = con.prepareStatement(sql);

            //3. Si tengo parámetros los llenamos y contamos a partir de 1 
            ps.setInt(1, idUsuario);
            ps.setInt(2, idPreferencia);
            ps.setInt(3, valor);

            //4. Se ejecuta y se recupera el id asociado al insert
            int cant = ps.executeUpdate();
            return cant > 0;

        } catch (Exception e) {
            throw new RuntimeException("Problemas al registrar una preferencia");
        }
    }

}
